" Convert filenames from 'path/to/file{.h,.cpp,_test.cpp}' to 'path/to/file'
function! zoox#FilegroupBasename()
  let l:fname = expand('%')
  if l:fname =~ '_test\.cpp'
    return l:fname[:-10]
  endif
  return expand('%:r')
endfunction

" Jump to build file and seach for filename where this command was called
function! zoox#JumpToBuildFile()
  let l:current_file = expand("%:t")
  let l:pattern = "\\V\\<" . l:current_file . "\\>"
  exe "edit" findfile("BUILD", ".;")
  call search(l:pattern, "w", 0, 500)
endfunction

" Jump to source/header/test/BUILD file
" See :help filename-modifiers
command! Bld :call zoox#JumpToBuildFile()
command! Src :exe "e " . zoox#FilegroupBasename() . ".cpp"
command! Hdr :exe "e " . zoox#FilegroupBasename() . ".h"
command! Tst :exe "e " . zoox#FilegroupBasename() . "_test.cpp"
