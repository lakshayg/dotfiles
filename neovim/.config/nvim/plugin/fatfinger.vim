" I never use Ex mode and commandline window
nnoremap Q  :echo "Ex mode has been disabled in fatfinger.vim!"<cr>
nnoremap q: :echo "Command window has been disbled in fatfinger.vim!"<cr>

" It is easy to press <F1> accidentally on the macbook touchbar
map <F1> <esc>
imap <F1> <esc>

command! Vs vs
command! Sp sp

" commands from https://vim.fandom.com/wiki/Handle_common_command_typos
command! -bang -nargs=? -complete=file E e<bang> <args>
command! -bang -nargs=? -complete=file W w<bang> <args>
command! -bang -nargs=? -complete=file Wq wq<bang> <args>
command! -bang -nargs=? -complete=file WQ wq<bang> <args>
command! -bang -nargs=? -complete=file Wqa wqa<bang> <args>
command! -bang Wa wa<bang>
command! -bang WA wa<bang>
command! -bang Wqa wqa<bang>
command! -bang Q q<bang>
command! -bang QA qa<bang>
command! -bang Qa qa<bang>

" I sometimes hold <Shift> for too long in the visual mode
" which makes the screen jump. If I had to select to the end
" or beginning, I would rather use v[motion]
xnoremap <S-up> k
xnoremap <S-left> h
xnoremap <S-down> j
xnoremap <S-right> l
snoremap <S-up> <up>
snoremap <S-left> <left>
snoremap <S-down> <down>
snoremap <S-right> <right>
onoremap <S-up> k
onoremap <S-left> h
onoremap <S-down> j
onoremap <S-right> l
inoremap <S-up> <ESC>gH
inoremap <S-left> <ESC>gh
inoremap <S-down> <ESC>gH
inoremap <S-right> <ESC>gh
