" Toggle qflist --------------------------------------------------------------

function! GetBufferList()
  redir =>buflist
  silent! ls!
  redir END
  return buflist
endfunction

function! BufferOpen(bufname)
  let buflist = GetBufferList()
  for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
    if bufwinnr(bufnum) != -1
      return 1
    endif
  endfor
  return 0
endfunction

function! ToggleList(bufname, close_cmd, open_cmd)
  let buflist = GetBufferList()
  for bufnum in map(filter(split(buflist, '\n'), 'v:val =~ "'.a:bufname.'"'), 'str2nr(matchstr(v:val, "\\d\\+"))')
    if bufwinnr(bufnum) != -1
      exec(a:close_cmd)
      return
    endif
  endfor
  exec(a:open_cmd)
endfunction

" Persistent background setting ------------------------------------------------
" I prefer light schemes during the day and dark schemes during the night,
" this config helps me persist the background setting without modifying the
" config file all the time

let g:background_file="/tmp/nvim-background"

function! GetBackground()
  if filereadable(g:background_file)
    return trim(system("cat " . g:background_file))
  endif
  return "dark"
endfunction

function! SetBackground(value)
  call system("echo " . a:value . " > " . g:background_file)
  exe 'set background=' . a:value
endfunction

call SetBackground(GetBackground())

" Use more powerful external programs if possible ------------------------------

if executable("rg")
  set grepprg=rg\ --hidden\ --no-messages\ --vimgrep
  set grepformat^=%f:%l:%c:%m
endif

if executable("par")
  set formatprg=par
endif

" --hscroll-off=<very large value> ensures that if the file names
"  are long, we see the right end of the name rather than the left
"
" Based on my benchmarks, rg appears to be the faster than fd.
" See https://github.com/sharkdp/fd/issues/616
" git ls-files is extremely fast for git repos but since we also
" want to list untracked files, we need to call git ls-files with
" --others which slows it down quite a bit with untracked files
" (slower than rg). Note that these are two commands, combining
" them into a single command increases the time to begin output
" significantly. Another known caveat of this approach is that if
" a file has been deleted but has not been staged, git ls-files
" will still show it in the list of files
if executable("rg")
  let g:fzf_fallback_source = "rg --color=never --hidden --no-messages --files"
elseif executable("fd")
  let g:fzf_fallback_source = "fd --color=never --hidden --type f"
elseif executable("fdfind")
  let g:fzf_fallback_source = "fdfind --color=never --hidden --type f"
else
  let g:fzf_fallback_source = "find . -type f"
endif

" --recurse-submodules requires git version >= 2.14
let g:fzf_user_options = {
      \ 'options': ['--hscroll-off', '100000'],
      \ 'source': "git ls-files --recurse-submodules        2>/dev/null && "
      \ .         "git ls-files --others --exclude-standard 2>/dev/null || "
      \ .          g:fzf_fallback_source
      \ }

" Inspired by tpope/vim-unimpaired
" <space> commands create/close/toggle stuff
" ]x, [x move between the stuff
"
"  ________________________________________________________________________________________
" | x      | object   | mnemonic                                                           |
" |--------|----------|--------------------------------------------------------------------|
" | <spc>  | files    | no mnemonic, fzf is commonly used and <spc><spc> is quick to press |
" |--------|----------|--------------------------------------------------------------------|
" | b      | buffers  | "b"uffer                                                           |
" |--------|----------|--------------------------------------------------------------------|
" | t      | tags     | "t"ags                                                             |
" |--------|----------|--------------------------------------------------------------------|
" | q      | quickfix | "q"uickfix                                                         |
" |--------|----------|--------------------------------------------------------------------|
" | =      | tabs     | + is on the = key. =/- were chosen because they represent creation |
" | -      |          | and deletion and they lie in the number row, which is at the top,  |
" |        |          | just like tabs                                                     |
" |--------|----------|--------------------------------------------------------------------|
" | /      |background| ? looks like a lightbulb, lies above the / key, switche light/dark |
" +--------|----------|--------------------------------------------------------------------+
"

nnoremap        <silent> <space><space> :call fzf#run(fzf#wrap(g:fzf_user_options))<cr>
nnoremap        <silent> <space>b       :Buffers<cr>
nnoremap        <silent> <space>t       :tselect<cr>
nnoremap <expr> <silent> <space>q       BufferOpen("Quickfix List") ? ":cclose<cr>" : ":vert copen 70<cr>"
nnoremap        <silent> <space>=       :tab sbuffer %<cr>
nnoremap        <silent> <space>-       :tabclose<cr>
nnoremap        <silent> <space>/       :call SetBackground(&background == "dark" ? "light" : "dark")<cr>

nnoremap <silent> ]b :bnext<cr>
nnoremap <silent> [b :bprev<cr>
nnoremap <silent> ]t :tnext<cr>
nnoremap <silent> [t :tprev<cr>
nnoremap <silent> ]q :cnext<cr>
nnoremap <silent> [q :cprev<cr>
nnoremap <silent> ]= :tabnext<cr>
nnoremap <silent> ]- :tabnext<cr>
nnoremap <silent> [= :tabprev<cr>
nnoremap <silent> [- :tabprev<cr>

"
" Split settings
"
nnoremap <C-Down>   <C-W><C-J>
nnoremap <C-Up>     <C-W><C-K>
nnoremap <C-Right>  <C-W><C-L>
nnoremap <C-Left>   <C-W><C-H>
tnoremap <C-Left>   <C-\><C-N><C-w>h
tnoremap <C-Down>   <C-\><C-N><C-w>j
tnoremap <C-Up>     <C-\><C-N><C-w>k
tnoremap <C-Right>  <C-\><C-N><C-w>l
nnoremap <S-Left>   :vert resize -5<cr>
nnoremap <S-Right>  :vert resize +5<cr>
nnoremap <S-Up>     :resize +3<cr>
nnoremap <S-Down>   :resize -3<cr>
" Use <Esc> to exit terminal mode
tnoremap <Esc>      <C-\><C-n>
" The tnoremap mapping above messes up fzf's exit on esc
" behaviour. This autocmd solves the issue
augroup UnimpairedMappings
  autocmd!
  autocmd FileType fzf tnoremap <buffer> <esc> <c-c>
augroup END
