function! GithubNormalizeRemote(remote)
  let l:norm = a:remote
  if l:norm =~ "\.git$"
    let l:norm = l:norm[0:-5]
  endif
  " for repos where the remote is of the form git@domain.com:owner/repo
  if l:norm =~ "@"
    let l:parts = split(l:norm, '@')
    let l:norm = "http://" . substitute(l:parts[1], ":", "/", "")
  endif
  return l:norm
endfunction

function! GithubLink()
  let l:git = "git -C " . expand("%:p:h") . " "
  let l:remote = GithubNormalizeRemote(trim(system(l:git . "remote get-url origin")))
  let l:branch = trim(system(l:git . "rev-parse --abbrev-ref HEAD"))
  if l:branch == "HEAD"
    " Can happen if the HEAD is pointing to some unnamed commit
    let l:branch = trim(system(l:git . "rev-parse HEAD"))
  endif
  " Path to current file relative to git root
  let l:relpath = trim(system(l:git . "ls-files --full-name " . expand("%:p")))
  if v:shell_error != 0
    echohl WarningMsg
    echom "Cannot run git commands!"
    echohl None
    return ""
  else
    return printf("%s/blob/%s/%s", l:remote, l:branch, l:relpath)
  endif
endfunction

function! GithubLinkLine()
  return printf("%s#L%d", GithubLink(), line("."))
endfunction

function! GithubLinkRange()
  return printf("%s#L%d-L%d", GithubLink(), getpos("'<")[1], getpos("'>")[1])
endfunction

if !exists('g:github_open_cmd')
  if has('mac')
    let g:github_open_cmd = 'open'
  elseif executable('xdg-open')
    let g:github_open_cmd = 'xdg-open'
  endif
endif

function! GithubOpen(url)
  let l:cmd = g:github_open_cmd . " " . a:url
  call system(l:cmd)
  if v:shell_error != 0
    echohl WarningMsg
    echom "Error executing: " . l:cmd
    echohl None
  endif
endfunction

" yank the github link
if has('clipboard')
  nnoremap <silent> <leader>gy :let @"=GithubLinkLine() <bar> let @+=@"<cr>
  vnoremap <silent> <leader>gy :<c-u>let @"=GithubLinkRange() <bar> let @+=@"<cr>
else
  nnoremap <silent> <leader>gy :let @"=GithubLinkLine()<cr>
  vnoremap <silent> <leader>gy :<c-u>let @"=GithubLinkRange()<cr>
endif

" open github link in the browser
nnoremap <silent> <leader>gh :call GithubOpen(GithubLinkLine())<cr>
vnoremap <silent> <leader>gh :<c-u>call GithubOpen(GithubLinkRange())<cr>

" ==============================================================================
" Test Cases
" ==============================================================================
" 1. Works with remotes of the form http://github.com/USER/REPO
" 2. Works with remotes of the form git@github.com:USER/REPO
" 3. Works from outside the git repo
" 4. Works from inside git repo in a subdirectory
" 5. Works when the checked out commit does not have an associated branch name
" 6. Report error when git commands cannot be run
" 7. Report error when open commands cannot be run
" 8. <leader>gy yanks to vim and system clipboards in visual and normal modes
