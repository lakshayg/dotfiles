highlight LineNr       cterm=None    ctermfg=None  ctermbg=None gui=None    guifg=None  guibg=None
highlight SignColumn   cterm=None    ctermfg=None  ctermbg=None gui=None    guifg=None  guibg=None
highlight DiffAdd      cterm=reverse ctermfg=Green ctermbg=None gui=reverse guifg=Green guibg=None
highlight DiffDelete   cterm=reverse ctermfg=Red   ctermbg=None gui=reverse guifg=Red   guibg=None
highlight DiffChange   cterm=reverse ctermfg=Blue  ctermbg=None gui=reverse guifg=Blue  guibg=None
highlight CursorLine   cterm=None    ctermfg=None  ctermbg=None gui=None    guifg=None  guibg=None
highlight CursorLineNR cterm=reverse ctermfg=None  ctermbg=None gui=reverse guifg=None  guibg=None
highlight VertSplit    cterm=None    ctermfg=None  ctermbg=None gui=None    guifg=None  guibg=None
