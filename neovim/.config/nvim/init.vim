call plug#begin()
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'             " :FZF, :Rg, :Files, :Tags
Plug 'tpope/vim-fugitive'           " Using this primarily for Gdiff and Gblame
Plug 'PeterRincker/vim-searchlight' " Highlight the current match. !!!NOTE!!! This plugin slows down the search in large files if neovim (not vim)
Plug 'markonm/traces.vim'           " highlights patterns and ranges for Ex commands in Command-line mode, similar to inccommand but better

Plug 'kassio/neoterm'
let g:neoterm_autoscroll = 1

" sheerun/vim-polyglot {{{
Plug 'sheerun/vim-polyglot'         " Language plugin for over 100 langs
let g:polyglot_disabled = ['csv']
" }}}

" wellle/context.vim {{{
let g:context_enabled = 0
Plug 'wellle/context.vim', { 'on': ['ContextPeek', 'ContextEnable', 'ContextToggle'] } " Alternative to approx_scope.py from my old dotfiles
" }}}

" wellle/targets.vim {{{
" Most of the text objects are as one would expect. The most
" interesting ones are:
" 1. a - function (a)rgument
" 2. b - any (b)lock | B - curly (b)race
" 3. q - any (q)uote
" 4. , - between two commas
Plug 'wellle/targets.vim'           " adds various text objects
" }}}

" mhinz/vim-signify {{{
" Show git diff markers in SignColumn
" This has been pinned to the specified commit because the
" latest version (c3d450eb5f5e76d99d23b10493d4e08c5bb1ae71)
" seems to have an issue which prevents vim-signify from
" showing markers if I toggle the signcolumn
Plug 'mhinz/vim-signify', { 'commit': '2fd4951d43eeac5ebc3c662fa003cac812852a43' }
" }}}

" majutsushi/tagbar {{{
Plug 'majutsushi/tagbar', { 'on': 'TagbarToggle' } " Show tags in the current file
command! TT :TagbarToggle
" }}}

" vim-scripts/AmbiCompletion {{{
Plug 'vim-scripts/AmbiCompletion'
set completefunc=g:AmbiCompletion
" }}}

" lifepillar/vim-mucomplete {{{
Plug 'lifepillar/vim-mucomplete'    " Bind <Tab> and <S-Tab> to vim's internal autocomplete capabilities
set shortmess+=c
let g:mucomplete#always_use_completeopt = 1
let g:mucomplete#enable_auto_at_startup = 0
let g:mucomplete#chains = {
    \ 'default' : ['path', 'c-p', 'omni', 'user'],
    \ 'vim'     : ['path', 'cmd', 'keyn', 'user'],
    \ 'markdown': ['path', 'c-p', 'uspl', 'dict'],
    \ }
" }}}

" tpope/vim-commentary {{{
Plug 'tpope/vim-commentary'         " gcc, gc[motion]
nmap // gcc
vmap // gc
" }}}

" morhetz/gruvbox {{{
Plug 'morhetz/gruvbox'              " A nice colorscheme
if ($COLORTERM == "truecolor" || $COLORTERM == "24bit" || $TERM =~ "256color")
  set termguicolors
  let g:gruvbox_termcolors = 256
else
  let g:gruvbox_termcolors = 16
endif
let g:gruvbox_contrast_dark = 'medium'
let g:gruvbox_contrast_light = 'hard'
let g:gruvbox_invert_selection = 0
" }}}

" lakshayg/vim-bazel {{{
Plug 'lakshayg/vim-bazel', { 'branch': 'develop' }
nnoremap \bb :Bazel build<cr>
nnoremap \bt :Bazel test<cr>
nnoremap \br :Bazel run<cr>
" }}}
call plug#end()

packadd cfilter " Provides Cfilter and Cfilter! to filter quickfix list

" Note: To use the meta key in iterm2,
" iterm2 > Preferences > Profiles > Keys
" and change Left, Right Option key to send Esc+

syntax enable               " enable syntax processing
filetype plugin indent on   " load filetype-specific indent files

" General options {{{
set title           " Set window title based on active buffer
set number          " show line numbers
set showmatch       " highlight matching parentheses
set nowrap          " no text wrapping by default, "set wrap" to enable
set scrolloff=5     " keep a few lines of context around the cursor when scrolling
set sidescroll=1
set sidescrolloff=5
set updatetime=750  " Refresh time for plugins like vim-signify and tagbar
set lazyredraw      " redraw only when we need to (for faster macros)
set autoread        " Re-read a file if it has been changed outside nvim
set virtualedit=block " Allow placing cursor in places with no text in virtual block mode
set mouse=a         " enable mouse support for normal, visual, insert and cmdline modes
set splitbelow      " more natural splitting
set splitright
set report=0        " Always report the number of lines changed
set spellfile=$HOME/.config/nvim/spelling.utf-8.add " zg - set good word, zw - set wrong word

" Use system keyboard by default, this currently does not work well
" with visual block https://github.com/neovim/neovim/issues/1822
" set clipboard+=unnamedplus

" Wildignore path is meant to ignore the directories from recursive
" search which we almost never want to look at.
set wildignore+=*/.git/*,
set wildignore+=*/bazel-out/*,*/bazel-bin/*,*/bazel-testlogs/*,

" Turn on wild menu for command line completion support. Vim cmds can
" now be completed by pressing Tab key.
set wildmenu
set wildmode=full
" }}}

" Custom mappings and commands {{{
" fix syntax highlighting when it goes out of sync
map <F2> :syntax sync fromstart<cr>

" Takes the contents of register " (which is essentially the vim clipboard)
" and puts them into the system clipboard. Handles the case when the server
" is being accessed over ssh
let g:clipboard_sync_command =
      \  " if [[ -z $SSH_CONNECTION ]]; then                "
      \. "   ( pbcopy || xsel -i ) 2> /dev/null;            "
      \. " else                                             "
      \. "   ssh $(echo $SSH_CONNECTION | cut -d' ' -f1) \  "
      \. "     '( pbcopy || xsel -i ) 2> /dev/null';        "
      \. " fi                                               "
command! SyncClipboard :echo system(g:clipboard_sync_command, @")
nnoremap <F3> :SyncClipboard<cr>
" }}}

" Search and Replace {{{
"
" Makes * and # work on visual mode too.
" Source: https://github.com/godlygeek/vim-files/blob/master/plugin/vsearch.vim
" Modified to keep cursor at the same place when searching
function! VSetSearch()
  let temp = @s
  " Yank current visual selection into the s register
  norm! gv"sy
  let @/ = '\V' . substitute(escape(@s, '\'), '\n', '\\n', 'g')
  call histadd('/', substitute(@/, '[?/]', '\="\\%d".char2nr(submatch(0))', 'g'))
  let @s = temp
endfunction

set hlsearch            " highlight matches
set incsearch           " search as characters are entered
set ignorecase          " Ignore case when searching
set smartcase           " Ignore the ignorecase option when search pattern contains uppercase
if !has_key(g:plugs, 'traces.vim') " traces.vim is incompatible with inccommand
  set inccommand=nosplit  " Live preview when replacing text
endif

nnoremap <silent> * :let @/= '\<' . expand('<cword>') . '\>' <bar> set hlsearch<cr>
xnoremap <silent> * :<C-u>call VSetSearch() <bar> set hlsearch<cr>
nnoremap <silent> <esc> :noh<return><esc>
nmap <expr> <2-LeftMouse> "*"
" }}}

" Diff {{{
set diffopt+=foldcolumn:0         " No FoldCoumn in the diff mode
set diffopt+=algorithm:histogram  " Use the histogram diffing algorithm
set diffopt+=indent-heuristic     " Use indent heuristic for internal diff library
set diffopt+=vertical             " Show vertical diffs by default
" }}}

" Appearance {{{
" Highlight characters I don't want in the code
set list
set listchars=tab:»-,trail:·,nbsp:×
set fillchars=vert:┃              " Use thin vertical split separator
set cursorline                    " Highlight current line
colorscheme gruvbox
" }}}

" Indentation {{{
set shiftwidth=2   " number of spaces to use for autoindent
set softtabstop=2  " number of spaces in tab when editing
set expandtab      " automatically expand tabs to spaces
set smarttab       " <Tab> at line start inserts `shiftwidth` no of blanks
set autoindent     " Copy indent from current line to new line
" }}}

" Autocompletion {{{
" No ignorecase for omni-completion
let g:omni_syntax_ignorecase = 0

" syntaxcomplete#Complete seems to have problems working properly
" when ignorecase is set in neovim (not vim). This function sets
" noignorecase before calling syntaxcomplete#Complete and works
" around the issue
function! SyntaxComplete(findstart, base)
  let l:ignorecase_value = &l:ignorecase
  let &l:ignorecase = 0
  let l:results = syntaxcomplete#Complete(a:findstart, a:base) 
  let &l:ignorecase = l:ignorecase_value
  return l:results
endfunction

set completeopt=menuone,noselect,preview
set belloff+=ctrlg        " No beeps during completion
" Search buffers for completion options. By default vim also searches
" include files and tags file, it is slow when dealing with large
" projects and simple completions are good enough for me
set complete=.,w,b,u
" }}}

" Status line {{{
set statusline=%<%f               " Filename, possibly truncated at the start
set statusline+=\ %y              " Filetype flag
set statusline+=\ %h%m%r          " [help], modified, readonly flags
set statusline+=%=                " Separator
set statusline+=\ %-9.(%l,%c%V%)  " Line and column numbers
" }}}

" Autocommands {{{
augroup Vimrc
  autocmd!

  " Open the quickfix list whenever it is populated by some command
  " This autocmd needs to be nested to work properly with vim-qfedit
  autocmd QuickFixCmdPost [^l]* nested cwindow
  autocmd QuickFixCmdPost    l* nested lwindow
  autocmd VimEnter            * cwindow

  " Hide signcolumn and disable syntax in diff mode (vim-signify)
  autocmd OptionSet diff
        \ if &diff
        \ |   setlocal signcolumn=no
        \ |   setlocal syntax=off
        \ | else
        \ |   setlocal signcolumn=auto
        \ |   setlocal syntax=on
        \ | endif

  " A better diff colorscheme
  autocmd ColorScheme *
        \   highlight DiffAdd    cterm=None ctermfg=Green  ctermbg=None gui=None guifg=LightGreen guibg=bg
        \ | highlight DiffDelete cterm=None ctermfg=Red    ctermbg=None gui=None guifg=Pink       guibg=bg
        \ | highlight DiffChange cterm=None ctermfg=Brown  ctermbg=None gui=None guifg=None       guibg=bg
        \ | highlight DiffText   cterm=bold ctermfg=Yellow ctermbg=None gui=None guifg=bg         guibg=Orange

  " Adjustment to gruvbox
  autocmd ColorScheme *
        \   highlight CursorLine cterm=None ctermfg=None   ctermbg=None gui=None guifg=None   guibg=None

  " When editing a file, always jump to the last known cursor position.
  " Don't do it when the position is invalid, when inside an event
  " handler (happens when dropping a file on gvim) and for a commit
  " message (it's likely a different one than last time).
  autocmd BufReadPost *
        \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
        \ |   exe "normal! g`\""
        \ | endif

  " I find the default autocomplete too noisy, this omnicomplete
  " function just uses the the keywords from syntax files. This
  " needs to be an autocmd so that it applies after the ftplugin
  " has done its thing otherwise omnifunc is set by ftplugin
  autocmd FileType * set omnifunc=SyntaxComplete

augroup END
" }}}

" Load $VIMPROFILE {{{
if !empty($VIMPROFILE)
  let profile_file = "profiles/" . $VIMPROFILE . ".vim"
  if empty(globpath(&runtimepath, profile_file))
    echo "Cannot load profile settings from " . profile_file . ", file not found"
  else
    runtime profiles/$VIMPROFILE.vim
  endif
endif
" }}}

set secure exrc
" vim:foldmethod=marker
