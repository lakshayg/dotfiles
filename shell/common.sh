# vim mode for bash/zsh
set -o vi

if [[ $TERM == "xterm-kitty" ]]; then
  alias diff="kitty +kitten diff"
  alias gd="git difftool -t kitty -y --trust-exit-code --no-symlinks --dir-diff"

  # A number of applications behave weird when they see
  # TERM=xterm-kitty. Changing it to xterm to keep those
  # applications happy
  export TERM="xterm"
fi

if [[ $OSTYPE =~ ^"darwin" ]]; then
  alias RestartTouchBar="sudo pkill TouchBarServer; sudo killall ControlStrip"
fi

# Nodejs
if which npm > /dev/null; then
  export NODE_PATH="$(npm root -g npm):$NODE_PATH"
fi

# Golang
if which go > /dev/null && [ -z "$GOPATH" ] && [ -d "$HOME/go" ]; then
  export GOPATH="$HOME/go"
fi

if [ ! -z "$GOPATH" ]; then
  export PATH="$GOPATH/bin:$PATH"
fi
