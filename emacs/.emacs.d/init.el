; GUI related options
(add-to-list 'default-frame-alist '(font . "Monaco-14"))

(menu-bar-mode -1)
(toggle-scroll-bar -1)
(tool-bar-mode -1)

(setq visible-bell t
      ring-bell-function 'ignore)
(setq mac-command-modifier 'meta) ; use command key as meta
(set-default 'truncate-lines t)
(setq grep-command "git --no-pager grep -n -e "
      grep-use-null-device nil)

; Startup options
(setq initial-major-mode 'lisp-interaction-mode)
(custom-set-variables
 '(inhibit-startup-screen t))

; Editing experience
(global-display-line-numbers-mode)
(show-paren-mode 1)
